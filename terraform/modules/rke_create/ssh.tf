# Provides a Hetzner Cloud SSH key resource to manage SSH keys for server access:
# https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/ssh_key

resource "hcloud_ssh_key" "management" {
  name       = "rancher-management-key"
  public_key = file("${var.hcloud_ssh_key_path}.pub")

  labels = {
    "project-name" = var.project_name,
    "builder"      = "terraform",
  }
}
