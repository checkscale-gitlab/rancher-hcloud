resource "hcloud_load_balancer" "rancher" {
  name               = var.lb_name
  load_balancer_type = var.lb_type
  location           = var.lb_location

  dynamic "target" {
    for_each = hcloud_server.rancher
    content {
      type      = "server"
      server_id = target.value.id
    }
  }

  labels = {
    "lb-role"      = "rancher-management",
    "project-name" = var.project_name,
    "builder"      = "terraform",
  }
}

resource "hcloud_load_balancer_network" "rancher" {
  load_balancer_id = hcloud_load_balancer.rancher.id
  subnet_id        = hcloud_network_subnet.rancher.id
}

resource "hcloud_load_balancer_target" "rancher" {
  count            = var.instance_count
  type             = "server"
  load_balancer_id = hcloud_load_balancer.rancher.id
  server_id        = hcloud_server.rancher[count.index].id
  use_private_ip   = true
  depends_on       = [hcloud_load_balancer_network.rancher]
}

resource "hcloud_load_balancer_service" "rancher_k8s_service" {
  load_balancer_id = hcloud_load_balancer.rancher.id
  protocol         = "tcp"
  listen_port      = 6443
  destination_port = 6443
  depends_on       = [hcloud_load_balancer_target.rancher]
}

resource "hcloud_load_balancer_service" "rancher_http_service" {
  load_balancer_id = hcloud_load_balancer.rancher.id
  protocol         = "tcp"
  listen_port      = 80
  destination_port = 80
  depends_on       = [hcloud_load_balancer_target.rancher]
}

resource "hcloud_load_balancer_service" "rancher_https_service" {
  load_balancer_id = hcloud_load_balancer.rancher.id
  protocol         = "tcp"
  listen_port      = 443
  destination_port = 443
  depends_on       = [hcloud_load_balancer_target.rancher]
}
