variable "letsencrypt_issuer" {
  type    = string
  default = null
}

variable "rancher_admin_password" {
  type    = string
  default = null
}

variable "kubeconfig_path" {
  type    = string
  default = null
}

variable "kubeconfig_yaml" {
  type    = string
  default = null
}

variable "lb_address" {
  type    = string
  default = null
}

variable "hcloud_token" {
  type    = string
  default = null
}

variable "network_name" {
  type    = string
  default = "rancher"
}

variable "os_image" {
  type    = string
  default = "ubuntu-20.04"
}

variable "project_domain" {
  type    = string
  default = null
}
