#
# Cluster Config
#
docker_root_dir: /var/lib/docker
enable_cluster_alerting: false
enable_cluster_monitoring: false
enable_network_policy: false
local_cluster_auth_endpoint:
  enabled: true
#
# Rancher Config
#
rancher_kubernetes_engine_config:
  addon_job_timeout: 45
  authentication:
    strategy: x509
  cloud_provider:
    name: external
  dns:
    nodelocal:
      ip_address: ''
      node_selector: null
      update_strategy: {}
  enable_cri_dockerd: false
  ignore_docker_version: true
  #
  # # Currently only nginx ingress provider is supported.
  # # To disable ingress controller, set `provider: none`
  # # To enable ingress on specific nodes, use the node_selector, eg:
  #    provider: nginx
  #    node_selector:
  #      app: ingress
  #
  ingress:
    default_backend: false
    http_port: 0
    https_port: 0
    provider: nginx
  kubernetes_version: 1.21.6
  monitoring:
    provider: metrics-server
    replicas: 1
  #
  #   If you are using calico on AWS
  #
  #    network:
  #      plugin: calico
  #      calico_network_provider:
  #        cloud_provider: aws
  #
  # # To specify flannel interface
  #
  #    network:
  #      plugin: flannel
  #      flannel_network_provider:
  #      iface: eth1
  #
  # # To specify flannel interface for canal plugin
  #
  #    network:
  #      plugin: canal
  #      canal_network_provider:
  #        iface: eth1
  #
  network:
    mtu: 0
    options:
      flannel_backend_type: vxlan
    plugin: calico
  rotate_encryption_key: false
  #
  #    services:
  #      kube-api:
  #        service_cluster_ip_range: 10.43.0.0/16
  #      kube-controller:
  #        cluster_cidr: 10.42.0.0/16
  #        service_cluster_ip_range: 10.43.0.0/16
  #      kubelet:
  #        cluster_domain: cluster.local
  #        cluster_dns_server: 10.43.0.10
  #
  services:
    etcd:
      backup_config:
        enabled: true
        interval_hours: 12
        retention: 6
        safe_timestamp: false
        timeout: 300
      creation: 12h
      extra_args:
        election-timeout: 5000
        heartbeat-interval: 500
      gid: 0
      retention: 72h
      snapshot: false
      uid: 0
    kube_api:
      always_pull_images: false
      pod_security_policy: false
      secrets_encryption_config:
        enabled: false
      service_node_port_range: 30000-32767
  ssh_agent_auth: false
  upgrade_strategy:
    max_unavailable_controlplane: '1'
    max_unavailable_worker: 10%
    node_drain_input:
      delete_local_data: false
      force: false
      grace_period: -1
      ignore_daemon_sets: true
      timeout: 120
  addons: |-
    apiVersion: v1
    kind: Secret
    metadata:
      name: hcloud-csi
      namespace: kube-system
    stringData:
      token: MY_HCLOUD_TOKEN
    ---
    apiVersion: v1
    kind: Secret
    metadata:
      name: hcloud
      namespace: kube-system
    stringData:
      token: MY_HCLOUD_TOKEN
  addonsInclude:
    - https://raw.githubusercontent.com/hetznercloud/csi-driver/v1.6.0/deploy/kubernetes/hcloud-csi.yml
    - https://github.com/hetznercloud/hcloud-cloud-controller-manager/releases/latest/download/ccm.yaml
windows_prefered_cluster: false
