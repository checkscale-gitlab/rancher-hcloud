module "rke_create" {
  source              = "./modules/rke_create"
  hcloud_token        = var.hcloud_token
  dns_token           = var.dns_token
  hcloud_ssh_key_path = var.hcloud_ssh_key_path
  instance_count      = var.instance_count
  instance_prefix     = var.instance_prefix
  instance_type       = var.instance_type
  instance_zones      = var.instance_zones
  lb_location         = var.lb_location
  lb_name             = var.lb_name
  lb_type             = var.lb_type
  network_name        = var.network_name
  project_domain      = var.project_domain
}

module "rancher_init" {
  source = "./modules/rancher_init"

  hcloud_token           = var.hcloud_token
  letsencrypt_issuer     = var.letsencrypt_issuer
  rancher_admin_password = var.rancher_admin_password
  network_name           = var.network_name
  project_domain         = var.project_domain

  kubeconfig_path = module.rke_create.kubeconfig_path
  kubeconfig_yaml = module.rke_create.kubeconfig_yaml
  lb_address      = module.rke_create.lb_address
}
